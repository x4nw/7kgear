# Flash using the on-chip bootloader over USB-UART
# The UART must be specified as an argument to this script.
# Put the board in bootloader mode by pressing the BOOTLOADER button
# (may not work if you press it too fast - if this fails, try holding it a
# bit)

stm32flash -w build/7kcalfw-f411.hex -v -g 0x0 -b 115200 "$1"
