#!/bin/bash

JLinkExe -ExitOnError 1 -NoGui 1 <<"EOF"
device STM32F411VE
if SWD
speed 4000
connect
erase
loadfile ./build/7kcalfw-f411.hex
reset
go
quit
EOF
