/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2024 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

#include "stm32f4xx_ll_i2c.h"
#include "stm32f4xx_ll_rcc.h"
#include "stm32f4xx_ll_bus.h"
#include "stm32f4xx_ll_system.h"
#include "stm32f4xx_ll_exti.h"
#include "stm32f4xx_ll_cortex.h"
#include "stm32f4xx_ll_utils.h"
#include "stm32f4xx_ll_pwr.h"
#include "stm32f4xx_ll_dma.h"
#include "stm32f4xx_ll_spi.h"
#include "stm32f4xx_ll_tim.h"
#include "stm32f4xx_ll_usart.h"
#include "stm32f4xx_ll_gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */
#define T7KCAL_VER "t7kcal_hw1_fw1"

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define USART_Com USART1
#define PM2_Pin LL_GPIO_PIN_2
#define PM2_GPIO_Port GPIOE
#define PM3_Pin LL_GPIO_PIN_3
#define PM3_GPIO_Port GPIOE
#define EnXCompInhibit_Pin LL_GPIO_PIN_1
#define EnXCompInhibit_GPIO_Port GPIOC
#define EnForceReadout_Pin LL_GPIO_PIN_2
#define EnForceReadout_GPIO_Port GPIOC
#define SigEnExt_Pin LL_GPIO_PIN_0
#define SigEnExt_GPIO_Port GPIOA
#define SigPulse_Pin LL_GPIO_PIN_1
#define SigPulse_GPIO_Port GPIOA
#define TrigPulse_Pin LL_GPIO_PIN_2
#define TrigPulse_GPIO_Port GPIOA
#define SigSelectPLL_Pin LL_GPIO_PIN_4
#define SigSelectPLL_GPIO_Port GPIOA
#define TrigEnExt_Pin LL_GPIO_PIN_5
#define TrigEnExt_GPIO_Port GPIOA
#define TrigSelectPLL_Pin LL_GPIO_PIN_6
#define TrigSelectPLL_GPIO_Port GPIOA
#define IAChannel_Pin LL_GPIO_PIN_7
#define IAChannel_GPIO_Port GPIOA
#define IAMode_Pin LL_GPIO_PIN_4
#define IAMode_GPIO_Port GPIOC
#define IAGain2_Pin LL_GPIO_PIN_5
#define IAGain2_GPIO_Port GPIOC
#define nTS1_Pin LL_GPIO_PIN_0
#define nTS1_GPIO_Port GPIOB
#define nTS2_Pin LL_GPIO_PIN_1
#define nTS2_GPIO_Port GPIOB
#define nTS3_Pin LL_GPIO_PIN_2
#define nTS3_GPIO_Port GPIOB
#define IAGain1_Pin LL_GPIO_PIN_7
#define IAGain1_GPIO_Port GPIOE
#define IAGain0_Pin LL_GPIO_PIN_8
#define IAGain0_GPIO_Port GPIOE
#define IAnCS_Pin LL_GPIO_PIN_9
#define IAnCS_GPIO_Port GPIOE
#define SlowSignal_Pin LL_GPIO_PIN_10
#define SlowSignal_GPIO_Port GPIOE
#define SlowTrigger_Pin LL_GPIO_PIN_11
#define SlowTrigger_GPIO_Port GPIOE
#define ADCDAC_nSYNC_Pin LL_GPIO_PIN_12
#define ADCDAC_nSYNC_GPIO_Port GPIOB
#define ADCDAC_SCLK_Pin LL_GPIO_PIN_13
#define ADCDAC_SCLK_GPIO_Port GPIOB
#define ADCDAC_CIPO_Pin LL_GPIO_PIN_14
#define ADCDAC_CIPO_GPIO_Port GPIOB
#define ADCDAC_COPI_Pin LL_GPIO_PIN_15
#define ADCDAC_COPI_GPIO_Port GPIOB
#define RData8_Pin LL_GPIO_PIN_8
#define RData8_GPIO_Port GPIOD
#define RData9_Pin LL_GPIO_PIN_9
#define RData9_GPIO_Port GPIOD
#define RData10_Pin LL_GPIO_PIN_10
#define RData10_GPIO_Port GPIOD
#define RData11_Pin LL_GPIO_PIN_11
#define RData11_GPIO_Port GPIOD
#define RData12_Pin LL_GPIO_PIN_12
#define RData12_GPIO_Port GPIOD
#define RData13_Pin LL_GPIO_PIN_13
#define RData13_GPIO_Port GPIOD
#define RData14_Pin LL_GPIO_PIN_14
#define RData14_GPIO_Port GPIOD
#define RData15_Pin LL_GPIO_PIN_15
#define RData15_GPIO_Port GPIOD
#define ADCDAC_nRESET_Pin LL_GPIO_PIN_6
#define ADCDAC_nRESET_GPIO_Port GPIOC
#define PLL_SDA_Pin LL_GPIO_PIN_9
#define PLL_SDA_GPIO_Port GPIOC
#define PLL_SCL_Pin LL_GPIO_PIN_8
#define PLL_SCL_GPIO_Port GPIOA
#define ComRX_Pin LL_GPIO_PIN_9
#define ComRX_GPIO_Port GPIOA
#define ComTX_Pin LL_GPIO_PIN_10
#define ComTX_GPIO_Port GPIOA
#define RRAM_nOE_Pin LL_GPIO_PIN_11
#define RRAM_nOE_GPIO_Port GPIOC
#define RRAM_nWE_Pin LL_GPIO_PIN_12
#define RRAM_nWE_GPIO_Port GPIOC
#define RData7_Pin LL_GPIO_PIN_0
#define RData7_GPIO_Port GPIOD
#define RData6_Pin LL_GPIO_PIN_1
#define RData6_GPIO_Port GPIOD
#define RData5_Pin LL_GPIO_PIN_2
#define RData5_GPIO_Port GPIOD
#define RData4_Pin LL_GPIO_PIN_3
#define RData4_GPIO_Port GPIOD
#define RData3_Pin LL_GPIO_PIN_4
#define RData3_GPIO_Port GPIOD
#define RData2_Pin LL_GPIO_PIN_5
#define RData2_GPIO_Port GPIOD
#define RData1_Pin LL_GPIO_PIN_6
#define RData1_GPIO_Port GPIOD
#define RData0_Pin LL_GPIO_PIN_7
#define RData0_GPIO_Port GPIOD
#define nTS4_Pin LL_GPIO_PIN_3
#define nTS4_GPIO_Port GPIOB
#define nTS5_Pin LL_GPIO_PIN_4
#define nTS5_GPIO_Port GPIOB
#define nTS6_Pin LL_GPIO_PIN_5
#define nTS6_GPIO_Port GPIOB
#define nTS7_Pin LL_GPIO_PIN_6
#define nTS7_GPIO_Port GPIOB
#define nTS8_Pin LL_GPIO_PIN_7
#define nTS8_GPIO_Port GPIOB
#define nTS9_Pin LL_GPIO_PIN_8
#define nTS9_GPIO_Port GPIOB
#define nTS10_Pin LL_GPIO_PIN_9
#define nTS10_GPIO_Port GPIOB
#define PM0_Pin LL_GPIO_PIN_0
#define PM0_GPIO_Port GPIOE
#define PM1_Pin LL_GPIO_PIN_1
#define PM1_GPIO_Port GPIOE

/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
