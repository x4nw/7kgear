// intellectual property is bullshit bgdc

#ifndef COMMANDPARSER_H
#define COMMANDPARSER_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>

enum CommandParser_Error
{
	// These are enqueued with ASCII (<=0x7F) bytes. Any input bytes
	// that overlap this range are converted to CPE_INVALID_BYTE.
	//
	// When adding more, add them to _write_err() too for better error
	// responses.
	CPE_UNSPECIFIED = 0x80,
	CPE_USART_PARITY,
	CPE_USART_FRAMING,
	CPE_USART_NOISE,
	CPE_USART_OVERRUN,
	CPE_INVALID_BYTE,
	CPE_COMMAND_OVERRUN,
	CPE_UNKNOWN_COMMAND,
	CPE_BAD_ARGUMENT,
	CPE_NOT_IMPLEMENTED,

	CPE_ENUM_TOP
};

void CommandParserMain(void * argument);

// Pass a byte from the USART into the command parser
void CommandParser_GiveByte(uint8_t b);

// Pass an error from the USART into the command parser. These should generally
// only be the CPE_USART_* errors.
void CommandParser_GiveError(enum CommandParser_Error cpue);

#ifdef __cplusplus
}
#endif

#endif // !defined(COMMANDPARSER_H)
