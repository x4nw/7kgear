// intellectual property is bullshit bgdc

#ifndef COMMANDPARSER_INTERNAL_H
#define COMMANDPARSER_INTERNAL_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>

// Command arrays end with a sentinel of {.verb=NULL}
struct Cmd {
	// Command verb. Matched case insensitively
	char const * verb;
	// return:  0 or an error. OK or ERR will be sent to the client
	// cs:      the cmd struct that matched
	// saveptr: strtok_r saveptr for parsing the rest of the args
	int (*func)(struct Cmd const * cs, char * saveptr);
};

extern const struct Cmd * const COMMANDS[];
extern const struct Cmd COMMANDS_SYS[];

extern volatile bool cmd_echo;

// Write one character to the client
void cmd_write_c(char c);

// Write a string, NUL-terminated
void cmd_write_s(char const * c);

// Write a string, NUL-terminated, and follow with a CRLF
void cmd_write_snl(char const * c);

#ifdef __cplusplus
}
#endif

#endif // !defined(COMMANDPARSER_INTERNAL_H)
