// intellectual property is bullshit bgdc

#include "CommandParser.h"
#include "CommandParser_Internal.h"
#include "cmsis_os.h"
#include "main.h"

#include <assert.h>
#include <ctype.h>
#include <inttypes.h>
#include <stdatomic.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define NUM_QUEUE_BYTES 64
#define NUM_CMD_BYTES 64

volatile bool cmd_echo = true;

static atomic_bool        _initialized = false;
static osMessageQueueId_t _mq_serial_in;
static atomic_bool        _mq_serial_in_overrun;

static void _handle_cmd(char * cmd);
static void _handle_err(enum CommandParser_Error err);
static void _write_ok(void);
static void _write_err(enum CommandParser_Error);

const struct Cmd * const COMMANDS[] = {
	COMMANDS_SYS,
	NULL
};

void CommandParserMain(void * argument)
{
	static char              cmd_buf[NUM_CMD_BYTES];
	size_t                   cmd_len = 0;
	enum CommandParser_Error cmd_err = 0;

	static const osMessageQueueAttr_t attr = {
		.name      = "mqSerIn",
		.attr_bits = 0,
		.cb_mem    = NULL,
		.cb_size   = 0,
		.mq_mem    = NULL,
		.mq_size   = 0,
	};

	_mq_serial_in = osMessageQueueNew(NUM_QUEUE_BYTES, 1, &attr);
	assert(_mq_serial_in);

	LL_USART_EnableIT_RXNE(USART_Com);
	LL_USART_EnableIT_PE(USART_Com);
	LL_USART_EnableIT_ERROR(USART_Com);

	_initialized = true;

	for (;;)
	{
		uint8_t b;
		osStatus_t st = osMessageQueueGet(
			_mq_serial_in,
			&b,
			NULL,
			osWaitForever
		);

		if (st != osOK)
		{
			cmd_len = 0;
			cmd_err = 0;
			_handle_err(CPE_UNSPECIFIED);
			continue;
		}

		if (b < CPE_UNSPECIFIED && cmd_echo)
		{
			cmd_write_c(b);
			if (b == '\r')
				cmd_write_c('\n');
		}

		if (b == '\n' || b == '\r')
		{
			if (cmd_err)
				_handle_err(cmd_err);
			else if (cmd_len >= sizeof(cmd_buf))
				_handle_err(CPE_COMMAND_OVERRUN);
			else
			{
				cmd_buf[cmd_len] = 0;
				_handle_cmd(cmd_buf);
			}
			cmd_len = 0;
			cmd_err = 0;
		}
		else if (b == '\x03')
		{
			// Ctrl-C resets state
			cmd_len = 0;
			cmd_err = 0;
		}
		else if (cmd_len < sizeof(cmd_buf))
		{
			if (isspace(b))
			{
				// All whitespace is collapsed into a single sp
				// (and initial whitespace is ignored)
				if (cmd_len && cmd_buf[cmd_len - 1] != ' ')
				{
					cmd_buf[cmd_len++] = ' ';
				}
			}
			else if (isprint(b))
			{
				cmd_buf[cmd_len++] = b;
			}
			else
			{
				cmd_err = b >= CPE_UNSPECIFIED
					? b
					: CPE_INVALID_BYTE;
			}
		}
		else if (b >= CPE_UNSPECIFIED)
		{
			cmd_err = b;
		}
	}
}

void CommandParser_GiveByte(uint8_t b)
{
	if (!_initialized)
		return;

	if (b < 0x80)
	{
		osStatus_t st = osMessageQueuePut(_mq_serial_in, &b, 0, 0);
		if (st != osOK)
			_mq_serial_in_overrun = true;
	}
	else
	{
		CommandParser_GiveError(CPE_INVALID_BYTE);
	}
}

void CommandParser_GiveError(enum CommandParser_Error cpue)
{
	if (!_initialized)
		return;

	int n_error = (int) cpue;

	if (n_error < 0x80 || n_error > 0xFF)
		n_error = CPE_UNSPECIFIED;

	uint8_t error_byte = (uint8_t) n_error;

	osStatus_t st = osMessageQueuePut(_mq_serial_in, &error_byte, 0, 0);
	if (st != osOK)
		_mq_serial_in_overrun = true;
}

static void _handle_cmd(char * cmd)
{
	char * saveptr = NULL;
	char * verb = strtok_r(cmd, " ", &saveptr);

	if (!verb)
	{
		_write_err(CPE_UNKNOWN_COMMAND);
		return;
	}

	for (size_t i = 0; COMMANDS[i]; i++)
	{
		for (size_t j = 0; COMMANDS[i][j].verb; j++)
		{
			struct Cmd const * cmd = & COMMANDS[i][j];
			if (strcasecmp(cmd->verb, verb) == 0)
			{
				int rtn = cmd->func(cmd, saveptr);
				if (rtn == 0)
					_write_ok();
				else
					_write_err(rtn);
				return;
			}
		}
	}

	_write_err(CPE_UNKNOWN_COMMAND);
}

static void _handle_err(enum CommandParser_Error err)
{
	_write_err(err);
}

void cmd_write_c(char c)
{
	while (!LL_USART_IsActiveFlag_TXE(USART_Com));

	LL_USART_TransmitData8(USART_Com, c);
}

void cmd_write_s(char const * s)
{
	for (size_t i = 0; s && s[i]; i++)
		cmd_write_c(s[i]);
}

void cmd_write_snl(char const * s)
{
	cmd_write_s(s);
	cmd_write_s("\r\n");
}

static void _write_ok(void)
{
	cmd_write_s("OK\r\n");
}

static void _write_err(enum CommandParser_Error err)
{
	char err_s[32];

	if (err < CPE_UNSPECIFIED || err >= CPE_ENUM_TOP)
		err = CPE_UNSPECIFIED;

	snprintf(err_s, sizeof(err_s), "ERR %02X", (uint8_t) err);
	cmd_write_s(err_s);

	switch (err)
	{
	case CPE_UNSPECIFIED:     cmd_write_snl(" UNSPECIFIED"); break;
	case CPE_USART_PARITY:    cmd_write_snl(" USART_PARITY"); break;
	case CPE_USART_FRAMING:   cmd_write_snl(" USART_FRAMING"); break;
	case CPE_USART_NOISE:     cmd_write_snl(" USART_NOISE"); break;
	case CPE_USART_OVERRUN:   cmd_write_snl(" USART_OVERRUN"); break;
	case CPE_INVALID_BYTE:    cmd_write_snl(" INVALID_BYTE"); break;
	case CPE_COMMAND_OVERRUN: cmd_write_snl(" COMMAND_OVERRUN"); break;
	case CPE_UNKNOWN_COMMAND: cmd_write_snl(" UNKNOWN_COMMAND"); break;
	case CPE_BAD_ARGUMENT:    cmd_write_snl(" BAD_ARGUMENT"); break;
	case CPE_NOT_IMPLEMENTED: cmd_write_snl(" NOT_IMPLEMENTED"); break;
	default: cmd_write_snl("");
	}
}
