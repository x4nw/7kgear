// intellectual property is bullshit bgdc

#include "CommandParser.h"
#include "CommandParser_Internal.h"
#include "cmsis_os.h"
#include "main.h"

#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>
#include <string.h>
#include <stdlib.h>

static int _cmd_bootloader(struct Cmd const *, char *);
static int _cmd_echo(struct Cmd const *, char *);
static int _cmd_list(struct Cmd const *, char *);
static int _cmd_ver (struct Cmd const *, char *);

const struct Cmd COMMANDS_SYS[] = {
	// Go to ROM bootloader
	{ .verb = "bootloader", .func = _cmd_bootloader },
	// Turn echo on (1) or off (0)
	{ .verb = "echo", .func = _cmd_echo },
	// List all commands
	{ .verb = "list", .func = _cmd_list },
	// Print T7KCAL_VER
	{ .verb = "ver", .func = _cmd_ver },
	{ .verb = NULL }
};

static int _cmd_bootloader(struct Cmd const * cs, char * saveptr)
{
	return CPE_NOT_IMPLEMENTED;
}

static int _cmd_echo(struct Cmd const * cs, char * saveptr)
{
	char * arg = strtok_r(NULL, " ", &saveptr);

	if (!arg)
		return CPE_BAD_ARGUMENT;

	cmd_echo = (bool) strtol(arg, NULL, 10);
	return 0;
}

static int _cmd_list(struct Cmd const * cs, char * saveptr)
{
	for (size_t i = 0; COMMANDS[i]; i++)
	{
		for (size_t j = 0; COMMANDS[i][j].verb; j++)
			cmd_write_snl(COMMANDS[i][j].verb);
	}

	return 0;
}

static int _cmd_ver(struct Cmd const * cs, char * saveptr)
{
	cmd_write_snl(T7KCAL_VER);
	return 0;
}
