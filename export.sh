#!/bin/bash
#
for i in 7kcalibrator flextender-{plug,socket}; do
	pushd $i
	kicad_export $i -O export --clear \
		--schem --markdown-bom \
		--gerbers --gerb-n-layers=4 --gerb-pack --gerb-render --ibom
	popd
done
